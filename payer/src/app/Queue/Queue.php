<?php namespace Payer\Queue;

interface Queue
{
    public function push($data);

    public function shift(callable $callback);
}