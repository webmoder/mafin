<?php namespace Payer\Queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQQueue implements Queue
{
    /**
     * @var AMQPStreamConnection
     */
    private $connection;
    /**
     * @var string
     */
    private $queueName;

    public function __construct(AMQPStreamConnection $connection, string $queueName)
    {
        $this->connection = $connection;
        $this->queueName = $queueName;
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function push($data)
    {
        $channel = $this->connection->channel();

        $channel->queue_declare($this->queueName, false, false, false, false);

        $channel->basic_publish(
            new AMQPMessage(json_encode($data, JSON_PRESERVE_ZERO_FRACTION)),
            '',
            $this->queueName
        );

        $channel->close();
    }

    public function shift(callable $callback)
    {
        $channel = $this->connection->channel();

        $channel->queue_declare($this->queueName, false, false, false, false);

        $channel->basic_consume($this->queueName, '', false, true, false, false, function ($msg) use ($callback) {
            $callback(json_decode($msg->body));
        });

        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }
}