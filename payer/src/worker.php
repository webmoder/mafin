<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Payer\Queue\RabbitMQQueue;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('rabbit', 5672, 'rabbitmq', 'rabbitmq');

$consumer = new RabbitMQQueue($connection, 'orders');
$producer = new RabbitMQQueue($connection, 'notifications');

$consumer->shift(function ($order) use ($producer) {
    if (\Payer\PayService::pay($order)) {
        $producer->push([
            'user_id' => $order->user_id,
            'order_id'=> $order->order_id,
        ]);
    }
});