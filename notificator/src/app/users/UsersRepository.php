<?php namespace Notificator\Users;

class UsersRepository
{
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function findById($userId): ?User
    {
        return $this->makeFromRaw($this->storage->get($userId));
    }

    public function save(User $user): self
    {
        $this->storage->set($user->getId(), $user->toArray());

        return $this;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function makeFromRaw($data): ?User
    {
        if (!isset($data['id']) || !isset($data['phone']) || !isset($data['pushToken'])) {
            return null;
        }

        return new User($data['id'], $data['phone'], $data['pushToken']);
    }
}