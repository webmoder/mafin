<?php namespace Notificator\Users;

class User
{
    private $id;
    private $phone;
    private $pushToken;

    public function __construct(int $id, string $phone, string $pushToken)
    {
        $this->id = $id;
        $this->phone = $phone;
        $this->pushToken = $pushToken;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getPushToken(): string
    {
        return $this->pushToken;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'phone' => $this->phone,
            'pushToken' => $this->pushToken,
        ];
    }
}