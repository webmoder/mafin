<?php namespace Notificator\Users;

interface Storage
{
    public function set($key, $value): self;

    public function get($key);
}