<?php namespace Notificator\Users;

class InMemoryStorage implements Storage
{
    private $storage = [];

    public function set($key, $value): Storage
    {
        $this->storage[$key] = $value;

        return $this;
    }

    public function get($key)
    {
        return $this->storage[$key] ?? null;
    }
}