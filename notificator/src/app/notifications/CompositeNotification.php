<?php namespace Notificator\Notifications;

use Notificator\Users\User;

class CompositeNotification implements Notification
{
    /**
     * @var Notification[]
     */
    private $notifications = [];


    public function send(User $user, $message)
    {
        foreach ($this->notifications as $notification) {
            $notification->send($user, $message);
        }
    }

    public function addNotification(Notification $notification): self
    {
        $this->notifications[] = $notification;

        return $this;
    }
}