<?php namespace Notificator\Notifications;

use Notificator\Users\User;

interface Notification
{
    public function send(User $user, $message);
}