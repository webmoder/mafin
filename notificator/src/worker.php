<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Notificator\Notifications\CompositeNotification;
use Notificator\Notifications\PushNotification;
use Notificator\Notifications\SmsNotification;
use Notificator\Users\InMemoryStorage;
use Notificator\Users\User;
use Notificator\Users\UsersRepository;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$repository = new UsersRepository(new InMemoryStorage);

$repository
    ->save(new User(1, '+7(777)000-00-00', '605ff764c617d3cd28dbbdd72be8f9a2'))
    ->save(new User(2, '+7(777)111-11-11', '49ae49a23f67c759bf4fc791ba842aa2'));

$notificator = new CompositeNotification();
$notificator
    ->addNotification(new SmsNotification)
    ->addNotification(new PushNotification)
;


$connection = new AMQPStreamConnection('rabbit', 5672, 'rabbitmq', 'rabbitmq');
$channel = $connection->channel();

$channel->queue_declare('notifications', false, false, false, false);

$callback = function ($msg) use ($repository, $notificator) {
    $data = json_decode($msg->body);

    if($user = $repository->findById($data->user_id)) {
        $notificator->send($user, "Заказ N{$data->order_id} оплачен!");
    }
};

$channel->basic_consume('notifications', '', false, true, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}