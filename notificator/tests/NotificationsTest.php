<?php namespace Tests;

use Notificator\Notifications\CompositeNotification;
use Notificator\Notifications\Notification;
use Notificator\Users\User;
use PHPUnit\Framework\TestCase;

class NotificationsTest extends TestCase
{
    /** @var Notification */
    private $notification;

    protected function setUp()
    {
        $this->notification = new class implements Notification
        {
            public static $notifications = [];

            public function send(User $user, $message)
            {
                self::$notifications[] = [$user, $message];
            }

            public function getNotifications(): array
            {
                return self::$notifications;
            }
        };
    }

    public function testCanSendNotificationsWithCompositeNotification()
    {
        $notification = new CompositeNotification;
        $notification
            ->addNotification($this->notification)
            ->addNotification($this->notification);

        $user = new User(1, '+7(777)000-00-00', '605ff764c617d3cd28dbbdd72be8f9a2');
        $message = 'notification message';

        $notification->send($user, $message);

        $this->assertCount(2, $this->notification->getNotifications());
        $this->assertSame([$user, $message], $this->notification->getNotifications()[0]);
        $this->assertSame([$user, $message], $this->notification->getNotifications()[1]);
    }
}