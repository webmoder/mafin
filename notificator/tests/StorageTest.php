<?php namespace Tests;

use Notificator\Users\InMemoryStorage;
use PHPUnit\Framework\TestCase;

class InMemoryStorageTest extends TestCase
{
    public function testCanSetValueInStorage()
    {
        $storage = new InMemoryStorage();

        $storage->set('test key', 'test value');

        $this->assertEquals('test value', $storage->get('test key'));
    }
}