<?php namespace Tests;

use Notificator\Users\InMemoryStorage;
use Notificator\Users\User;
use Notificator\Users\UsersRepository;
use PHPUnit\Framework\TestCase;

class UserRepositoryTest extends TestCase
{
    public function testCanSaveUsers()
    {
        $repository = new UsersRepository(new InMemoryStorage);
        $user1 = new User(1, '+7(777)000-00-00', '605ff764c617d3cd28dbbdd72be8f9a2');
        $user2 = new User(2, '+7(777)111-11-11', '49ae49a23f67c759bf4fc791ba842aa2');

        $repository
            ->save($user1)
            ->save($user2);

        $this->assertEquals($user1, $repository->findById($user1->getId()));
        $this->assertEquals($user2, $repository->findById($user2->getId()));
    }
}