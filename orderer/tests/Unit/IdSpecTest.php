<?php namespace Tests\Unit;

use Orderer\Specs\IdSpec;
use Orderer\Specs\Spec;

class IdSpecTest extends AbstractSpecTest
{
    function getSpec(): Spec
    {
        return new IdSpec;
    }

    function specDataProvider(): array
    {
        return [
            'negative id' => [-1, false],
            'zero val id' => [0, false],
            'float val id' => [0.5, false],
            'positive id' => [1, true],
            'positive string id' => ['1', false],
        ];
    }
}