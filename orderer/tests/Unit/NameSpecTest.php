<?php namespace Tests\Unit;

use Orderer\Specs\NameSpec;
use Orderer\Specs\Spec;

class NameSpecTest extends AbstractSpecTest
{

    function getSpec(): Spec
    {
        return new NameSpec(4, 10);
    }

    function specDataProvider(): array
    {
        return [
            'Normal name' => ['Name', true],
            'One symbol name' => ['a', false],
            'Min symbols name' => ['Four', true],
            'Max symbols name' => ['TenSymbols', true],
            'Over max symbols name' => ['Very big name for product', false],
            'Int name' => [1, false],
        ];
    }
}