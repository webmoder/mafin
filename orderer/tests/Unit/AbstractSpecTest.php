<?php namespace Tests\Unit;

use Orderer\Specs\Spec;

abstract class AbstractSpecTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param mixed $val
     * @param bool $satisfied
     *
     * @dataProvider specDataProvider
     */
    public function testSpec($val, bool $satisfied)
    {
        $this->assertEquals($satisfied, $this->getSpec()->isSatisfiedBy($val));
    }

    abstract function getSpec(): Spec;

    abstract function specDataProvider(): array;
}