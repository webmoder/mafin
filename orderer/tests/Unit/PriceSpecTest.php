<?php namespace Tests\Unit;

use Orderer\Specs\PriceSpec;
use Orderer\Specs\Spec;

class PriceSpecTest extends AbstractSpecTest
{

    function getSpec(): Spec
    {
        return new PriceSpec;
    }

    function specDataProvider(): array
    {
        return [
            'Negative int price' => [-1, false],
            'Negative float price' => [-1.00, false],
            'Zero int price' => [0, false],
            'Zero float price' => [0.00, true],
            'Positive int price' => [1, false],
            'Positive float price' => [1.00, true],
        ];
    }
}