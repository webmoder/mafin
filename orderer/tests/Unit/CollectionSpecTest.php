<?php namespace Tests\Unit;

use Orderer\Specs\CollectionSpec;
use Orderer\Specs\Spec;

class CollectionSpecTest extends AbstractSpecTest
{
    function getSpec(): Spec
    {
        /**
         * Спесификация для теста коллекций
         * Будет возвращать true если true :), иначе false
         */
        $spec = new class implements Spec
        {
            public function isSatisfiedBy($value): bool
            {
                return is_bool($value) && $value;
            }
        };

        return new CollectionSpec($spec);
    }

    function specDataProvider(): array
    {
        return [
            'collection with true' => [
                [true, true, true], true,
            ],
            'collection with mixed' => [
                [true, false, true], false,
            ],
            'collection with false' => [
                [false, false, false], false,
            ],
        ];
    }
}