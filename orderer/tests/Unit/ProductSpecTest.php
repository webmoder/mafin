<?php namespace Tests\Unit;

use Orderer\Specs\IdSpec;
use Orderer\Specs\NameSpec;
use Orderer\Specs\PriceSpec;
use Orderer\Specs\ProductSpec;
use Orderer\Specs\Spec;

class ProductSpecTest extends AbstractSpecTest
{

    function getSpec(): Spec
    {
        return new ProductSpec(new IdSpec, new NameSpec(3, 255), new PriceSpec());
    }

    function specDataProvider(): array
    {
        return [
            'valid product' => [
                [
                    'product_id' => 1,
                    'product_name' => 'Гречка',
                    'price' => 500.00,
                ],
                true,
            ],
            'valid product with other props' => [
                [
                    'product_id' => 1,
                    'product_name' => 'Гречка',
                    'price' => 500.00,
                    'some field' => 'some value',
                ],
                true,
            ],
            'invalid product structure' => [
                [
                    'product_name' => 'Гречка',
                ],
                false,
            ],
        ];
    }
}