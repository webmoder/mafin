<?php

namespace Tests\Api;

use Orderer\App;
use Orderer\Queue\Queue;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;

class OrderTest extends TestCase
{
    /**
     * @var App
     */
    private $app;

    public function setUp()
    {
        $this->app = new App();

        $this->app->getContainer()['Queue'] = $this->getQueueMock();
    }

    public function testCanMakeOrder()
    {
        $products = [
            [
                'product_id' => 1,
                'product_name' => 'Гречка',
                'price' => 500.00
            ],
            [
                'product_id' => 2,
                'product_name' => 'Рис',
                'price' => 250.00
            ]
        ];

        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/order',
            'CONTENT_TYPE' => 'application/json',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($products);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->get()->run(true);

        $this->assertSame(204, $response->getStatusCode());

        /** @var Queue $queue */
        $queue = $this->app->getContainer()->get('Queue');

        $queue->shift(function ($data) use($products){
            $this->assertArrayHasKey('user_id', $data);
            $this->assertArrayHasKey('order_id', $data);
            $this->assertEquals($products, $data['products']);
        });
    }

    public function testCanNotSendUnspecifiedBody()
    {
        $body = [
            [
                'product_id' => 1,
                'price' => 500.00
            ],
            [
                'product_id' => 2,
                'product_name' => 'Рис',
            ]
        ];

        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/order',
            'CONTENT_TYPE' => 'application/json',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($body);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->get()->run(true);

        $this->assertSame(400, $response->getStatusCode());
    }

    /**
     * @return \Closure
     */
    private function getQueueMock(): \Closure
    {
        return function () {
            return new class implements Queue
            {
                private $queue;

                public function __construct()
                {
                    $this->queue = new \SplQueue;
                }

                public function push($data)
                {
                    $this->queue->enqueue($data);
                }

                public function shift(callable $callback)
                {
                    $callback($this->queue->dequeue());
                }
            };
        };
    }
}