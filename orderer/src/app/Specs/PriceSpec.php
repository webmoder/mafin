<?php namespace Orderer\Specs;

class PriceSpec implements Spec
{
    public function isSatisfiedBy($value): bool
    {
        return is_float($value) && $value >= 0; //0 is free
    }
}