<?php

namespace Orderer\Specs;

interface Spec
{
    public function isSatisfiedBy($value): bool;
}