<?php namespace Orderer\Specs;

class CollectionSpec implements Spec
{
    /**
     * @var Spec
     */
    private $productSpec;

    public function __construct(Spec $productSpec)
    {
        $this->productSpec = $productSpec;
    }

    public function isSatisfiedBy($value): bool
    {
        return is_array($value) && $this->satisfyValues($value);
    }

    private function satisfyValues($values): bool
    {
        foreach ($values as $value) {
            if (!$this->productSpec->isSatisfiedBy($value)) {
                return false;
            }
        }

        return true;
    }
}