<?php namespace Orderer\Specs;

class IdSpec implements Spec
{
    public function isSatisfiedBy($value): bool
    {
        return is_int($value) && $value > 0;
    }
}