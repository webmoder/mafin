<?php namespace Orderer\Specs;

class ProductSpec implements Spec
{
    /**
     * @var Spec
     */
    private $idSpec;
    /**
     * @var Spec
     */
    private $nameSpec;
    /**
     * @var Spec
     */
    private $priceSpec;

    public function __construct(Spec $idSpec, Spec $nameSpec, Spec $priceSpec)
    {
        $this->idSpec = $idSpec;
        $this->nameSpec = $nameSpec;
        $this->priceSpec = $priceSpec;
    }

    public function isSatisfiedBy($value): bool
    {
        return isset($value['product_id']) && $this->idSpec->isSatisfiedBy($value['product_id'])
            && isset($value['product_name']) && $this->nameSpec->isSatisfiedBy($value['product_name'])
            && isset($value['price']) && $this->priceSpec->isSatisfiedBy($value['price']);
    }
}