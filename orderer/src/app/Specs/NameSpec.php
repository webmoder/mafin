<?php namespace Orderer\Specs;

class NameSpec implements Spec
{
    /**
     * @var int
     */
    private $minLen;
    /**
     * @var int
     */
    private $maxLen;

    public function __construct(int $minLen, int $maxLen)
    {
        $this->minLen = $minLen;
        $this->maxLen = $maxLen;
    }

    public function isSatisfiedBy($value): bool
    {
        return is_string($value) && strlen($value) >= $this->minLen && strlen($value) <= $this->maxLen;
    }
}