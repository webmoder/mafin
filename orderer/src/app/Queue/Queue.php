<?php namespace Orderer\Queue;

interface Queue
{
    public function push($data);

    public function shift(callable $callback);
}