<?php namespace Orderer;

use Orderer\Queue\Queue;
use Orderer\Queue\RabbitMQQueue;
use Orderer\Specs\CollectionSpec;
use Orderer\Specs\IdSpec;
use Orderer\Specs\NameSpec;
use Orderer\Specs\PriceSpec;
use Orderer\Specs\ProductSpec;
use Orderer\Specs\Spec;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;

class App
{
    /**
     * @var \Slim\App
     */
    private $app;
    /**
     * @var Container
     */
    private $container;

    public function __construct()
    {
        $this->app = new \Slim\App($this->setUpContainer());

        $this->app->add(function (Request $request, Response $response, callable $next) {
            return $request->getHeaderLine('Content-Type') === 'application/json'
                ? $next($request, $response)
                : $response->withStatus(400);
        });

        $this->registerRoutes();
    }

    public function get(): \Slim\App
    {
        return $this->app;
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    private function setUpContainer(): Container
    {
        $this->container = new Container;

        $this->container['Queue'] = function () {
            return new RabbitMQQueue(
                new AMQPStreamConnection('rabbit', 5672, 'rabbitmq', 'rabbitmq'),
                'orders'
            );
        };

        $this->container['ProductSpec'] = function () {
            return new ProductSpec(new IdSpec, new NameSpec(3, 255), new PriceSpec);
        };

        $this->container['CollectionSpec'] = function (Container $container) {
            return new CollectionSpec($container->get('ProductSpec'));
        };

        return $this->container;
    }

    protected function registerRoutes()
    {
        $this->app->post('/order', function (Request $request, Response $response) {
            $products = $request->getParsedBody();

            /** @var Queue $queue */
            $queue = $this->get('Queue');
            $userId = rand(1, 2);
            $queue->push([
                'user_id' => $userId,
                'order_id' => md5($userId . json_encode($products) . time()),
                'products' => $products,
            ]);

            return $response->withStatus(204);
        })->add(function (Request $request, Response $response, callable $next) {
            $products = $request->getParsedBody();
            /** @var Spec $collectionSpec */
            $collectionSpec = $this->get('CollectionSpec');

            if (!$collectionSpec->isSatisfiedBy($products)) {
                return $response->withStatus(400);
            }

            return $next($request, $response);
        });
    }
}